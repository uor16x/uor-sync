import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart' show Either, Option;
import 'package:provider/provider.dart';
import 'package:uor_sync_client/providers/auth.dart';
import 'package:uor_sync_client/theme.dart';

class AddAccount extends StatefulWidget {
  final VoidCallback? onClose;

  const AddAccount({super.key, this.onClose});

  State<AddAccount> createState() => _AddAccountState();
}

class _AddAccountState extends State<AddAccount> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final RegExp _emailRegex =
      RegExp(r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$');
  String? _emailError;
  String? _passwordError;
  bool _loading = false;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  bool _validateInputs() {
    bool hasError = false;
    setState(() {
      if (_emailController.text.length < 4 ||
          !_emailController.text.contains(_emailRegex)) {
        _emailError = 'Invalid email. Please enter a valid email address.';
        hasError = true;
      } else {
        _emailError = null;
      }

      if (_passwordController.text.length < 6) {
        _passwordError = 'Password must be at least 6 characters long';
        hasError = true;
      } else {
        _passwordError = null;
      }
    });
    return !hasError;
  }

  void _clearEmailError() {
    setState(() {
      _emailError = null;
    });
  }

  void _clearPasswordError() {
    setState(() {
      _passwordError = null;
    });
  }

  Future<void> _authenticateUser() async {
    if (!_validateInputs()) {
      return;
    }

    setState(() {
      _loading = true;
    });

    try {
      Option<String> userOption =
          await Provider.of<AuthProvider>(context, listen: false)
              .auth(_emailController.text, _passwordController.text);
      userOption.fold(() {
        Navigator.of(context).pop();
      }, (String error) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(error),
            backgroundColor: Colors.red,
          ),
        );
      });
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add account'),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(1.0),
          child: Container(
            color: mainColor,
            height: 1.0,
          ),
        ),
        leading: widget.onClose != null
            ? IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: widget.onClose,
              )
            : null,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          child: Column(
            children: [
              TextFormField(
                controller: _emailController,
                onChanged: (_) => _clearEmailError(),
                decoration: InputDecoration(
                  labelText: 'Email',
                  errorText: _emailError,
                ),
              ),
              TextFormField(
                controller: _passwordController,
                onChanged: (_) => _clearPasswordError(),
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Password',
                  errorText: _passwordError,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                  top: 10,
                ),
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: _authenticateUser,
                  child: const Text('Authenticate'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
