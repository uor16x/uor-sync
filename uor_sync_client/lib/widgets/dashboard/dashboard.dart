import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uor_sync_client/models/user.dart';
import 'package:uor_sync_client/providers/auth.dart';
import 'package:uor_sync_client/theme.dart';

class Dashboard extends StatefulWidget {
  final User user;

  const Dashboard({required this.user});

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  void listUsers() {
    var provider = Provider.of<AuthProvider>(context, listen: false);
    var users = provider.getUsers();
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Column(
          children: [
            for (var user in users)
              ListTile(
                  title: Text(user.email),
                  onTap: () {
                    provider.setCurrentUser(user);
                    Navigator.pop(context);
                  }),
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                provider.setCurrentUser(null);
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome, ${widget.user.email}"),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(1.0),
          child: Container(
            color: mainColor,
            height: 1.0,
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10), // Add padding to the right
            child: IconButton(
              icon: Icon(Icons.list),
              onPressed: listUsers,
            ),
          ),
        ],
      ),
      body: Center(
        child: Text("Hello"),
      ),
    );
  }
}
