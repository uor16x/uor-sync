import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import 'package:provider/provider.dart';
import 'package:uor_sync_client/models/user.dart';
import 'package:uor_sync_client/providers/auth.dart';
import 'package:uor_sync_client/theme.dart';
import 'package:uor_sync_client/widgets/dashboard/dashboard.dart';
import 'package:uor_sync_client/widgets/login/add_account.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AuthProvider()),
      ],
      child: MaterialApp(
        title: 'Uor Sync',
        debugShowCheckedModeBanner: false,
        theme: theme,
        home: Builder(
          builder: (context) {
            return Consumer<AuthProvider>(
              builder: (context, authProvider, child) {
                List<User> users = authProvider.getUsers();
                Option<User> currentUser = authProvider.getCurrentUser();
                if (users.isEmpty || authProvider.selectedUser.isNone()) {
                  return AddAccount(
                    onClose: users.isNotEmpty
                        ? () {
                            authProvider.setCurrentUser(users.first);
                          }
                        : null,
                  );
                }
                return currentUser.fold(
                  () => const AddAccount(),
                  (t) => Dashboard(
                    user: t,
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
