import 'package:flutter/material.dart';

const Color mainColor = Colors.white60;

final ThemeData theme = ThemeData.dark(
  useMaterial3: true,
).copyWith(
  primaryColor: mainColor,
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: mainColor,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
        EdgeInsets.all(10.0),
      ),
      backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
      foregroundColor: MaterialStateProperty.all<Color>(mainColor),
    ),
  ),
);
