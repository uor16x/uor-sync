import 'package:flutter/foundation.dart';
import 'package:fpdart/fpdart.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:uor_sync_client/models/user.dart' as local;

class AuthProvider extends ChangeNotifier {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final List<local.User> _users = [];
  Option<local.User> selectedUser = none();

  void setCurrentUser(local.User? user) {
    selectedUser = user == null ? none() : Some(user);
    notifyListeners();
  }

  Future<Option<String>> auth(String email, String password) async {
    try {
      UserCredential userCredential =
          await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      local.User user = local.User(
          id: userCredential.user!.uid, email: userCredential.user!.email!);
      _users.add(user);
      selectedUser = Some(user);
      notifyListeners();
      return none();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'invalid-credential') {
        try {
          UserCredential userCredential =
              await _firebaseAuth.createUserWithEmailAndPassword(
            email: email,
            password: password,
          );
          local.User user = local.User(
              id: userCredential.user!.uid, email: userCredential.user!.email!);
          _users.add(user);
          selectedUser = Some(user);
          notifyListeners();
          return none();
        } on FirebaseAuthException catch (e) {
          return Some("Failed to add new user: ${e.message}");
        }
      } else if (e.code == 'wrong-password') {
        return const Some('Wrong password. Please try again.');
      } else {
        return Some('Auth Error: ${e.message}');
      }
    } catch (e) {
      return Some('Auth Error: $e');
    }
  }

  Option<local.User> getCurrentUser() {
    return selectedUser;
  }

  List<local.User> getUsers() {
    return _users;
  }

  void logout() {
    _users.clear();
    selectedUser = none();
    notifyListeners();
  }
}
